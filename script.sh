#!/bin/sh

# This script analyzes the installed system.
# It requires a web server with PHP and SQLite running. In addition, if you
# do not have MediaWiki sources on your computer, the option 'install'
# downloads them for you.
# Please set the CONFIGURATION VARIABLES in ./test-gitmw-lib.sh
VERSION="1.0.0"
./uC.sh /etc/hostname
HOSTNAME=$(sudo cat "/etc/hostname")
echo $HOSTNAME

usage () {
	echo "usage: "
	echo "	./nisko test : Run analysis on this PC"
	echo "		show : Print out existing results without actually testing."
	echo "		clean : Clear all previous test results."
	echo "		retest : Run analysis overriding existing results."
	echo "		ver : Show version."
}

create_dir () {
	if [ -d /tmp/$HOSTNAME ]; then
		echo "Test results for this PC already exist. If you would like to print them, run this command with 'show' as argument. If you would like to override the results then run this command again with 'retest'".
		exit 1
	else
		mkdir -p /tmp/$HOSTNAME
		RESULTS="/tmp/$HOSTNAME"
		sudo chmod -R 777 /tmp/$HOSTNAME
	fi
}

install_serial () {
	@echo "Intalling the advantech serial driver..."
	@echo "Please wait..."
	$(shell ! [ -d /lib/modules/$(shell uname -r)/kernel/drivers/adv17v35x ] && mkdir /lib/modules/$(shell uname -r)/kernel/drivers/adv17v35x)
	$(shell [ -f adv17v35x.ko ]  && cp ./adv17v35x.ko /lib/modules/$(shell uname -r)/kernel/drivers/adv17v35x/ && depmod && modprobe adv17v35x)
	@echo "Done"
}

get_info () {
	rm -f /tmp/raw.json
	printf "{\n" >> /tmp/raw.json
	printf " \"Machine\" : {\n" >> /tmp/raw.json
	echo	" \"hostname\": \"$(cat /etc/hostname)\"," >> /tmp/raw.json
	echo	" \"kernel\": \"$(uname -r)\"," >> /tmp/raw.json
	echo	" \"kernel_full\": \"$(uname -a)\"," >> /tmp/raw.json
	echo	" \"mac_address\": {" >> /tmp/raw.json
	echo	" \"eno8303\": \"$(ifconfig eno8303 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')\"," >> /tmp/raw.json
	echo	" \"eno8403\": \"$(ifconfig eno8403 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')\"" >> /tmp/raw.json
	echo	" }"	>> /tmp/raw.json
	echo	" },"	>> /tmp/raw.json
	echo	" \"results\" : {"	>> /tmp/raw.json
	echo	" \"time\" : \"$(date +"[%G,%m,%e,%H,%M,%S]")\"," >> /tmp/raw.json
	echo	$(printf " \"serials_found\" : [ " && printf " \"%s\"," $(ls /dev | grep ttyAP) && printf "\"%s\"],\n" null) >> /tmp/raw.json
	echo	$(printf " \"drives_found\" : [ " && printf " \"%s\"," $(ls /dev | grep nvme.n1$) && printf "\"%s\"],\n" null) >> /tmp/raw.json
	echo	" \"drive_speeds\" : {"	>> /tmp/raw.json
	echo	" \"nvme0n1\" : \"7\","	>> /tmp/raw.json
	echo	" \"nvme0n2\" : \"7\","	>> /tmp/raw.json
	echo	" \"nvme0n3\" : \"7\""	>> /tmp/raw.json
	echo	"},"	>> /tmp/raw.json
	echo	" \"serial_ports\" : {"	>> /tmp/raw.json
	echo	" \"AP0\" : \"7\","	>> /tmp/raw.json
	echo	" \"AP1\" : \"7\","	>> /tmp/raw.json
	echo	" \"AP2\" : \"7\""	>> /tmp/raw.json
	echo	"}" >> /tmp/raw.json
	echo	"}" >> /tmp/raw.json
	echo	"}" >> /tmp/raw.json
	echo "1"
	sudo sed -i 's/,"null"\]/]/g' /tmp/raw.json
	echo "2"
	cat /tmp/raw.json | jq -r . > /tmp/$HOSTNAME/data.json
	echo "3"
	lsblk >> /tmp/$HOSTNAME/lsblk.txt
	echo "4"
	sudo dmesg > /tmp/$HOSTNAME/dmesg.txt
	echo "5"
	ls /dev > /tmp/$HOSTNAME/dev.txt
	echo "6"
	lspci -vvvv > /tmp/$HOSTNAME/lspci.txt
	echo "7"
	sudo blkid > /tmp/$HOSTNAME/blkid.txt
}


# Argument: test, delete, --help | -h
case "$1" in
	"test" | "-t")
		#uC.sh /etc/hostname
		create_dir
		get_info
		exit 0
		;;
	"show" | "-s")
		show
		exit 0
		;;
	"clean" | "-c")
		rm -rf /tmp/$HOSTNAME
		exit
		;;
	"retest"| "-f")
		rm -rf /tmp/$HOSTNAME
		create_dir
		get_info
		exit 0
		;;
	"ver"	| "-v")
		echo $VERSION
		exit 0
		;;
	"--help" | "-h")
		usage
		exit 0
		;;
	*)
		echo "Invalid argument: $1"
		usage
		exit 1
		;;
esac

#sudo fio --name=fill_disk --filename=/dev/nvme3n1 --filesize=500G --ioengine=libaio --direct=1 --verify=0 --randrepeat=0 --bs=128K --iodepth=64 --rw=randwrite --iodepth_batch_submit=64 --iodepth_batch_complete_max=64


