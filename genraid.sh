#!/bin/bash

version=" 1.03-240822"
# Some preliminary stuff:
uuid="7C28-C588"
#path="$(findmnt -n -o TARGET $(blkid -U $uuid ))/$(cat /etc/hostname | head -n 1)"
path="$/home/rafael/$(cat /etc/hostname | head -n 1)"
mkdir -p $path
ma1=$(ip link show | awk '/ether/ {print $2; exit}' | sed 's/://g')
u1="$(echo $(echo $(openssl rand -hex 4)$(printf "%x\n" $((1$(date +%I)-100)))$( cat /etc/hostname | tail -c 8 | sed 's/[^0-9a-fA-F]/b0/g')-${ma1:0:4}-${ma1:4:4}-${ma1:8:4}-7afae1bd04e$(openssl rand -hex 1 | cut -c1)) | tail -c 37 | tr '[:upper:]' '[:lower:]')"
u2="$(echo $(echo $(openssl rand -hex 4)$( cat /etc/hostname | tail -c 8 | sed 's/[^0-9a-fA-F]/b0/g')$(printf "%x\n" $((1$(date +%I)-100)))-${ma1:8:4}-${ma1:4:4}-${ma1:0:4}-$(openssl rand -hex 1 | cut -c1)7afae1bd04e) | tail -c 37 | tr '[:upper:]' '[:lower:]')"

box() {
    local input="$1"
    local bold="\033[1m"
    local underline="\033[4m"
    local reset="\033[0m"
    local length=${#input}
    local border_top="╥"
    local border_bottom="╨"
    local border_side="║"
    local border_line="═"

    # Create the top border
    echo -n "$border_top"
    for ((i=0; i<length+2; i++)); do
        echo -n "$border_line"
    done
    echo "$border_top"

    # Print the text with side borders
    echo -e "$border_side ${bold}${underline}${input}${reset} $border_side"

    # Create the bottom border
    echo -n "$border_bottom"
    for ((i=0; i<length+2; i++)); do
        echo -n "$border_line"
    done
    echo "$border_bottom"
}
nukeRAID() {
	# Getting rid of old RAID arrays:
	box "Nuking all active RAID arrays"
	mdadm --stop --scan
}

createRAID() {
box "Creating new RAID 0 array"
# Finding all the large drive in the system:
num=$(echo $(lsblk -b -o NAME,SIZE,MOUNTPOINT | awk -v min_size="7471345849845" '$2 >= 7471345849845 && $3 == "" {print "/dev/" $1}') | wc -w)

drives=($(lsblk -b -o NAME,SIZE,MOUNTPOINT | awk -v min_size="7471345849845" '$2 >= 7471345849845 && $3 == "" {print "/dev/" $1}'))
if [ ${#drives[@]} -lt 8 ]; then
  echo "Error: Less than 8 unmounted drives above 7TB found."
  exit 1
fi

for drive in "${drives[@]}"
do
   mdadm --zero-superblock $drive
   echo "Done $drive."
done


# Use only the first 8 drives
selected_drives=("${drives[@]:0:8}")

echo "Found $num drives: $(echo $(lsblk -b -o NAME,SIZE,MOUNTPOINT | awk -v min_size="7471345849845" '$2 >= 7471345849845 && $3 == "" {print "/dev/" $1}'))
"

echo "Creating new RAID 0 Array:"
mdadm --create --verbose /dev/md8 --level=0 --raid-devices=$num --chunk=128 -u $u1 "${selected_drives[@]}"

echo "Making the configuration persistent."

mdadm --detail --brief | tee /etc/mdadm/mdadm.conf
}

formatRAID() {
box "Formatting the array with ext4 file system."

mkfs.ext4 -L RAID -U $u2 /dev/md8
sudo partprobe
}

fstab() {
box "Creating 'fstab' entry"
sed -i '/\/ext/d' "/etc/fstab"
echo "# RAID0 Array mounted on /ext:" | tee -a /etc/fstab
echo "/dev/disk/by-uuid/$u2	/ext		ext4	defaults,nofail		0	1" | tee -a /etc/fstab
}

testRAID() {
#"Warning! - Destructive test. All data on the RAID array will be lost!"
box "Testing the array"

box "mdadm --detail" | tee /tmp/fioResults.log
sudo mdadm --detail --scan --verbose /dev/md8 | tee -a /tmp/fioResults.log
#sudo mkfs.ext4 /dev/md8 | tee -a /tmp/fioResults.log
box "fio" | tee -a /tmp/fioResults.log
sudo fio --filename=/dev/md8 --direct=1 --rw=write --bs=128k --ioengine=libaio --iodepth=64 --runtime=300 --numjobs=1 --time_based --group_reporting --name=iops-test-job | tee -a /tmp/fioResults.log
box "iostat" | tee -a /tmp/fioResults.log
sudo iostat -x | tee -a /tmp/fioResults.log
cp /tmp/fioResults.log $path/
}

mountRAID() {
box "Mounting the array".
mkdir -p /ext
mount "/dev/disk/by-uuid/$u2" /ext
}

case "$1" in
        all)
		nukeRAID
		createRAID
		testRAID
		formatRAID
		fstab
		mountRAID
            ;;
        notest)
		nukeRAID
		createRAID
		formatRAID
		fstab
		mountRAID
            ;;
        nuke)
		nukeRAID
            ;;
        recreate)
		nukeRAID
		createRAID
		formatRAID
            ;;
        format)
		formatRAID
            ;;
        ver)
		echo "Version: $version"
	    ;;
        fstab)
		fstab
            ;;
	test)
		echo "Warning! - Destructive test. All data on the RAID array will be lost!"
		testRAID
		formatRAID
            ;;

        *)
		echo "Usage: $0 {all|notest|nuke|recreate|format|fstab|test|ver}"
	exit 1
            ;;
esac
