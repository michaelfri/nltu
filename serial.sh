#!/bin/bash

# Define the serial port and baud rates to test
SERIAL_PORT="/dev/ttyS0" # replace with your serial port
BAUD_RATES=(9600 19200 38400 57600 115200) # add or remove rates as needed

# Function to perform the loopback test
loopback_test() {
    # Configure the serial port
    stty -F $SERIAL_PORT $1 raw -echo -echoe -echok
    
    # Send a test string to the serial port
    echo "Testing" > $SERIAL_PORT
    
    # Read the output from the serial port
    read_output=$(head -n 1 < $SERIAL_PORT)
    
    # Check if the received data matches the sent data
    if [ "$read_output" == "Testing" ]; then
        echo "true"
    else
        echo "false"
    fi
#!/bin/bash

# Define the serial port and baud rates to test
SERIAL_PORT="/dev/ttyS0" # replace with your serial port
BAUD_RATES=(9600 19200 38400 57600 115200) # add or remove rates as needed

# Function to perform the loopback test
loopback_test() {
    # Configure the serial port
    stty -F $SERIAL_PORT $1 raw -echo -echoe -echok
    
    # Send a test string to the serial port
    echo "Testing" > $SERIAL_PORT
    
    # Read the output from the serial port
    read_output=$(head -n 1 < $SERIAL_PORT)
    
    # Check if the received data matches the sent data
    if [ "$read_output" == "Testing" ]; then
        echo "true"
    else
        echo "false"
    fi
}

# Test the serial port at each baud rate
for rate in "${BAUD_RATES[@]}"; do
    echo "Testing $SERIAL_PORT at baud rate $rate:"
    loopback_test $rate
done
}

# Test the serial port at each baud rate
for rate in "${BAUD_RATES[@]}"; do
    echo "Testing $SERIAL_PORT at baud rate $rate:"
    loopback_test $rate
done
