#!/bin/sh

rm /tmp/morad.json
printf "{\n" >> /tmp/morad.json
printf "\t \"Machine\" : {\n" >> /tmp/morad.json
echo	-e " \t \t \"hostname\": \"$(cat /etc/hostname)\"," >> /tmp/morad.json
echo	-e " \t \t \"kernel\": \"$(uname -r)\"," >> /tmp/morad.json
echo	-e " \t \t \"kernel_full\": \"$(uname -a)\"," >> /tmp/morad.json
echo	-e " \t \t \"mac_address\": {" >> /tmp/morad.json
echo	-e " \t \t \t \"eno8303\": \"$(ifconfig eno8303 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')\"," >> /tmp/morad.json
echo	-e " \t \t \t \"eno8403\": \"$(ifconfig eno8403 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')\"" >> /tmp/morad.json
echo	-e "\t \t }"	>> /tmp/morad.json
echo	-e "\t },"	>> /tmp/morad.json
echo	-e " \"results\" : {"	>> /tmp/morad.json
echo	-e " \"time\" : \"$(date +"[%G,%m,%e,%H,%M,%S]")\"," >> /tmp/morad.json
echo	$(printf " \"serials_found\" : [ " && printf " \"%s\"," $(ls /dev | grep ttyAP) && printf "\"%s\"],\n" /dev/null) >> /tmp/morad.json
echo	$(printf " \"drives_found\" : [ " && printf " \"%s\"," $(ls /dev | grep nvme.n1$) && printf "\"%s\"],\n" /dev/null) >> /tmp/morad.json
echo	-e " \"drive_speeds\" : {"	>> /tmp/morad.json
echo	-e " \"nvme0n1\" : \"7\","	>> /tmp/morad.json
echo	-e " \"nvme0n2\" : \"7\","	>> /tmp/morad.json
echo	-e " \"nvme0n3\" : \"7\""	>> /tmp/morad.json
echo	-e "},"	>> /tmp/morad.json
echo	-e " \"serial_ports\" : {"	>> /tmp/morad.json
echo	-e " \"AP0\" : \"7\","	>> /tmp/morad.json
echo	-e " \"AP1\" : \"7\","	>> /tmp/morad.json
echo	-e " \"AP2\" : \"7\""	>> /tmp/morad.json
echo	-e "}" >> /tmp/morad.json
echo	-e "}" >> /tmp/morad.json
echo	-e "}" >> /tmp/morad.json



#echo "{" >> /tmp/morad.json
#echo ' "Machine" : {' >> /tmp/morad.json
#echo  ' { "Kernel": "$(uname -r)" }' >> /tmp/morad.json
#echo  ' { "hostname": "$(cat /etc/hostname)" }' >> /tmp/morad.json
#echo "}" >> /tmp/morad.json

#Test for Serial driver
#Test for GPU
#lsblk
#dmesg

#
