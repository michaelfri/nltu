#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/elfnote-lto.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;
BUILD_LTO_INFO;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0x2f0167ad, "module_layout" },
	{ 0x4a17ed66, "sysrq_mask" },
	{ 0xba8fbd64, "_raw_spin_lock" },
	{ 0x51ef18ae, "tty_flip_buffer_push" },
	{ 0xb5b54b34, "_raw_spin_unlock" },
	{ 0x649253e, "uart_insert_char" },
	{ 0x15ba50a6, "jiffies" },
	{ 0x6b10bee1, "_copy_to_user" },
	{ 0x13c49cc2, "_copy_from_user" },
	{ 0xd0da656b, "__stack_chk_fail" },
	{ 0x92997ed8, "_printk" },
	{ 0xa3e49023, "kmem_cache_alloc_trace" },
	{ 0xf24f1d68, "kmalloc_caches" },
	{ 0x92d5838e, "request_threaded_irq" },
	{ 0x68f31cbd, "__list_add_valid" },
	{ 0xde80cd09, "ioremap" },
	{ 0x63a59ff7, "uart_write_wakeup" },
	{ 0xb9e7429c, "memcpy_toio" },
	{ 0xa3ee70f9, "uart_handle_cts_change" },
	{ 0xc8bf63a9, "uart_handle_dcd_change" },
	{ 0xe2964344, "__wake_up" },
	{ 0xc1514a3b, "free_irq" },
	{ 0x3213f038, "mutex_unlock" },
	{ 0x4dfa8d4b, "mutex_lock" },
	{ 0x37a0cba, "kfree" },
	{ 0x4b750f53, "_raw_spin_unlock_irq" },
	{ 0xe1537255, "__list_del_entry_valid" },
	{ 0x8427cc7b, "_raw_spin_lock_irq" },
	{ 0x65487097, "__x86_indirect_thunk_rax" },
	{ 0xd35cce70, "_raw_spin_unlock_irqrestore" },
	{ 0x34db050b, "_raw_spin_lock_irqsave" },
	{ 0x5b8239ca, "__x86_return_thunk" },
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0x7d8b61e5, "pci_unregister_driver" },
	{ 0xeed4c84d, "uart_unregister_driver" },
	{ 0x949d2398, "__pci_register_driver" },
	{ 0xedc03953, "iounmap" },
	{ 0x3c0440e4, "uart_remove_one_port" },
	{ 0xb523fb4c, "pci_disable_device" },
	{ 0xfb578fc5, "memset" },
	{ 0xeb233a45, "__kmalloc" },
	{ 0x98dabf92, "pci_enable_device" },
	{ 0xcbd4898c, "fortify_panic" },
	{ 0x9166fada, "strncpy" },
	{ 0xe2d5255a, "strcmp" },
	{ 0x754d539c, "strlen" },
	{ 0x69acdf38, "memcpy" },
	{ 0xa916b694, "strnlen" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0x62a5505c, "uart_register_driver" },
	{ 0x2f341535, "uart_add_one_port" },
	{ 0x78534f62, "init_timer_key" },
	{ 0x9a664aa5, "uart_match_port" },
	{ 0xba765579, "uart_update_timeout" },
	{ 0xb6e83127, "uart_get_baud_rate" },
	{ 0x3cf85989, "mod_timer" },
	{ 0x7647726c, "handle_sysrq" },
	{ 0xb38b6186, "uart_try_toggle_sysrq" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("pci:v000013FEd0000000Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000000Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000000Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000000Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000000Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000009sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000014sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000015sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000016sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000063sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000061sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000062sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000064sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000001Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000018sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd00000019sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000001Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000001Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000001Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v000013FEd0000001Dsv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "149CF7F88C7BDFB24B8C81A");
MODULE_INFO(rhelversion, "9.3");
