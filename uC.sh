#!/bin/bash

# Check if a file is provided as an argument
if [ -z "$1" ]; then
  echo "Usage: $0 filename"
  exit 1
fi

# Read the content of the file
content=$(cat "$1")

# Check if all letters are uppercase
if [[ "$content" =~ ^[A-Z[:space:][:punct:][:digit:]]*$ ]]; then
  echo "All letters are uppercase."
  exit 0
else
  # Convert all letters to uppercase and save back to the file
  echo "$content" | tr '[:lower:]' '[:upper:]' > "$1"
  echo "Converted all letters to uppercase."
fi